# Ansible Collection - zarrenspry.percona

A collection of roles to assist with the installation of Percona tools and 
services onto a Linux target. This project is considered Alpha in its 
current state.

## Xtrabackup

Installs the Xtrabackup tools and qpress onto a Linux target. This role has
been tested against AlmaLinux 8 and Ubuntu Server 22.04 but should work with
all distributions within either the RedHat or Debian family.

### Variables

```json:table
{
    "fields": ["Variable", "Description", "Default Value"],
    "items": [
        {
            "Variable": "percona_release_major_version",
            "Description": "Percona target major version",
            "Default Value": "1"
        },
        {
            "Variable": "percona_release_minor_version",
            "Description": "Percona target minor version",
            "Default Value": "0"
        },
        {
            "Variable": "percona_release_patch_version",
            "Description": "Percona target patch version",
            "Default Value": "27"
        },
        {
            "Variable": "percona_target_component",
            "Description": "Percona target component. Can be either `release`, `testing` or `experimental`. See [this link](https://docs.percona.com/percona-software-repositories/repository-location.html) for further details.",
            "Default Value": "release"
        },
        {
            "Variable": "xtrabackup_version",
            "Description": "Xtrabackup target version.",
            "Default Value": "24"
        }
    ]
}
```

### Example Playbook

Install the default version to a target machine:
```yaml
---
- hosts: all
  roles:
    - role: zarrenspry.percona.xtrabackup
```

Install the `testing` component to a target machine:
```yaml
---
- hosts: all
  roles:
    - role: zarrenspry.percona.xtrabackup
  vars:
    percona_target_component: testing
```
